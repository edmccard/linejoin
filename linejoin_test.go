// Copyright (c) 2013 Ed McCardell
// Use of this source code is governed by the MIT
// license that can be found in the LICENSE file.

package linejoin

import (
	"bytes"
	"io"
	"testing"
)

func TestNoToken(t *testing.T) {
	output := new(bytes.Buffer)
	filter := NewWriter(output, "//DNL", "")
	io.WriteString(filter, "No\ntoken")
	filter.Flush()
	if output.String() != "No\ntoken" {
		t.Error("failed NoToken")
	}
}

func TestTokenNotNL(t *testing.T) {
	output := new(bytes.Buffer)
	filter := NewWriter(output, "//X", "")
	io.WriteString(filter, "Hello //XX\nthere")
	filter.Flush()
	if output.String() != "Hello //XX\nthere" {
		t.Error("failed TokenNotNL")
	}
}

func TestTokenN(t *testing.T) {
	output := new(bytes.Buffer)
	filter := NewWriter(output, "##", "")
	io.WriteString(filter, "With ##\ntoken")
	filter.Flush()
	if output.String() != "With token" {
		t.Error("failed TokenN")
	}
}

func TestTokenR(t *testing.T) {
	output := new(bytes.Buffer)
	filter := NewWriter(output, "##", "")
	io.WriteString(filter, "With ##\rtoken")
	filter.Flush()
	if output.String() != "With token" {
		t.Error("failed TokenR")
	}
}

func TestTokenRN(t *testing.T) {
	output := new(bytes.Buffer)
	filter := NewWriter(output, "##", "")
	io.WriteString(filter, "With ##\r\ntoken")
	filter.Flush()
	if output.String() != "With token" {
		t.Error("failed TokenRN", output.String())
	}
}

func TestMultiNL(t *testing.T) {
	output := new(bytes.Buffer)
	filter := NewWriter(output, "//X", "")
	io.WriteString(filter, "1//X\n2\n3")
	filter.Flush()
	if output.String() != "12\n3" {
		t.Error("failed MultiNL")
	}
}

func TestSplitRN(t *testing.T) {
	output := new(bytes.Buffer)
	filter := NewWriter(output, "@", "")
	io.WriteString(filter, "1@\r")
	io.WriteString(filter, "\n2")
	filter.Flush()
	if output.String() != "12" {
		t.Error("failed SplitNL")
	}
}

func TestMultiR(t *testing.T) {
	output := new(bytes.Buffer)
	filter := NewWriter(output, "@", "")
	io.WriteString(filter, "1@\r")
	io.WriteString(filter, "\r2")
	filter.Flush()
	if output.String() != "1\r2" {
		t.Error("failed SplitNL")
	}
}

func TestSplitToken(t *testing.T) {
	output := new(bytes.Buffer)
	filter := NewWriter(output, "$$", "")
	io.WriteString(filter, "A$")
	io.WriteString(filter, "$\nB")
	filter.Flush()
	if output.String() != "AB" {
		t.Error("failed SplitToken")
	}
}

func TestSplitTokenNL(t *testing.T) {
	output := new(bytes.Buffer)
	filter := NewWriter(output, "$$", "")
	io.WriteString(filter, "A$$")
	io.WriteString(filter, "\nB")
	filter.Flush()
	if output.String() != "AB" {
		t.Error("failed SplitTokenNL", output.String())
	}
}

func TestSplitNonToken(t *testing.T) {
	output := new(bytes.Buffer)
	filter := NewWriter(output, "$$", "")
	io.WriteString(filter, "A$")
	io.WriteString(filter, "X\nB")
	filter.Flush()
	if output.String() != "A$X\nB" {
		t.Error("failed SplitNonToken")
	}
}

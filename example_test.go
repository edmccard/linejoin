// Copyright (c) 2013 Ed McCardell
// Use of this source code is governed by the MIT
// license that can be found in the LICENSE file.

package linejoin_test

import (
	"bitbucket.org/edmccard/linejoin"
	"os"
	"text/template"
)

// This example demonstrates using line-joining directly from a template,
// by executing the template to a linejoin.Writer.
const example = `##
{{template "Foo"}}##
{{template "Bar"}}##
{{define "Foo"}}##
Only two lines
{{end}}##
{{define "Bar"}}##
of text.
{{end}}##
`

func Example_template() {
	t := template.Must(template.New("Example").Parse(example))

	// Create the filter.
	filter := linejoin.NewWriter(os.Stdout, "##", "")
	t.Execute(filter, nil)

	// Output:
	// Only two lines
	// of text.
}

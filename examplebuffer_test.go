// Copyright (c) 2013 Ed McCardell
// Use of this source code is governed by the MIT
// license that can be found in the LICENSE file.

package linejoin_test

import (
	"bitbucket.org/edmccard/linejoin"
	"bytes"
	"io"
	"os"
	"text/template"
)

// This example demonstrates using line-joining by preprocessing a template
// through a buffer.
const example2 = `{{/*Example*/ \}}
{{template "Foo" \}}
{{template "Bar" \}}
{{define "Foo" \}}
Only two lines
{{end \}}
{{define "Bar" \}}
of text.
{{end \}}
`

func Example_buffer() {
	b := new(bytes.Buffer)
	filter := linejoin.NewWriter(b, ` \}}`, "}}")
	io.WriteString(filter, example2)
	t := template.Must(template.New("Example").Parse(b.String()))
	t.Execute(os.Stdout, nil)

	// Output:
	// Only two lines
	// of text.
}

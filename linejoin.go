// Copyright (c) 2013 Ed McCardell
// Use of this source code is governed by the MIT
// license that can be found in the LICENSE file.

// Package linejoin implements a write filter (linejoin.Writer) that
// joins lines ending with a continuation token.
//
// It was designed to be used with templates, to allow a more natural
// template layout that does not add too much whitespace to the output.
package linejoin

import (
	"bytes"
	"io"
)

// A Writer is a filter that joins lines, by deleting any newlines
// which follow a given continuation token.
//
// The Writer buffers input internally, so clients should call Flush
// after the last call to Write.
type Writer struct {
	output io.Writer
	token  []byte
	subst  []byte
	bufLen int
	inRN   bool
}

// NewWriter returns a new linejoin.Writer.
//
//	output	the filter output
//	token	the continuation token
//	subst	the replacement token
//
func NewWriter(output io.Writer, token string, subst string) *Writer {
	w := &Writer{output: output, token: []byte(token), subst: []byte(subst)}
	return w
}

// Write writes p to the writer w, which filters the output by
// removing any newlines ("\r", "\r\n", or "\n") which are preceded by
// w's continuation token (which is then replaced with w's replacement
// token.)
func (w *Writer) Write(p []byte) (n int, err error) {
	n = len(p)
	// Did the last p[] end with '\r'?
	if w.inRN {
		if len(p) > 0 {
			w.inRN = false
			if p[0] == '\n' {
				p = p[1:]
			}
		}
	}
	// Did the last p[] end with a prefix of the token?
	if w.bufLen > 0 {
		for len(p) > 0 && w.bufLen < len(w.token) && p[0] == w.token[w.bufLen] {
			w.bufLen++
			p = p[1:]
		}
		if len(p) == 0 {
			return
		}
		// Not the complete token?
		if w.bufLen < len(w.token) {
			w.Flush()
		} else {
			// Complete but not followed by a newline?
			if p[0] != '\r' && p[0] != '\n' {
				w.Flush()
			} else {
				// Complete token followed by newline
				w.bufLen = 0
				p = p[w.checkNewline(p):]
			}
		}
	}
	// assert w.bufLen == 0 && !w.inRN
	for len(p) > 0 {
		nlPos := bytes.IndexAny(p, "\r\n")
		// No newlines?
		if nlPos == -1 {
			// Check if p[] ends with a prefix of the token
			tok := w.token
			for len(tok) > 0 {
				if bytes.HasSuffix(p, tok) {
					w.bufLen = len(tok)
					p = p[:len(p)-len(tok)]
					break
				}
				tok = tok[:len(tok)-1]
			}
			_, err = w.output.Write(p)
			return
		}
		nLen := w.checkNewline(p[nlPos:])
		if bytes.HasSuffix(p[:nlPos], w.token) {
			_, err = w.output.Write(p[:nlPos-len(w.token)])
			if err != nil {
				return
			}
			if len(w.subst) > 0 {
				_, err = w.output.Write(w.subst)
				if err != nil {
					return
				}
			}
		} else {
			_, err = w.output.Write(p[:nlPos+nLen])
			if err != nil {
				return
			}
		}
		p = p[nlPos+nLen:]
	}
	return
}

// Must not be called if p[0] is not a newline character
func (w *Writer) checkNewline(p []byte) (nLen int) {
	// assert !w.inRN && len(p) > 0 && (p[0] == '\r' || p[0] == '\n')
	if p[0] == '\r' {
		w.inRN = true
	}
	nLen = 1
	if len(p) > 1 && w.inRN && p[1] == '\n' {
		w.inRN = false
		nLen = 2
	}
	return
}

// Flush should be called after the last call to Write.
func (w *Writer) Flush() (err error) {
	if w.bufLen > 0 {
		_, err = w.output.Write(w.token[:w.bufLen])
		w.bufLen = 0
	}
	return
}

linejoin is a go package that implements a write filter (linejoin.Writer)
that joins lines ending with a continuation token.

It was designed to allow writing templates without having to cram things
together to avoid extra newlines in the output; for example, you can do this

    {{if .Foo \}}
    {{range .Bar \}}
    int {{.X}} = {{.Y}};
    {{end \}}
    {{end \}}

instead of

    {{if .Foo}}{{range .Bar}}
    int {{.X}} = {{.Y}};{{end}}{{end}}

See the [package docs](http://godoc.org/bitbucket.org/edmccard/linejoin)
for examples.
